package com.hcl.shoppingportal.util;

public class ShoppingPortalConstants {

	private ShoppingPortalConstants() {
		
	}
	public static final String PLACE_ORDER_START_IN_CONTROLLER = "start processing placeOrder() in controller";
	public static final String PLACE_ORDER_END_IN_CONTROLLER = "end processing placeOrder() in controller";
	public static final String ADD_TO_CART_START_IN_CONTROLLER = "start processing addProducts() in controller";
	public static final String SEARCH_PRODUCT_START_IN_CONTROLLER = "start processing searchProducts() in controller";
	public static final String SEARCH_PRODUCT_END_IN_CONTROLLER = "end processing searchProducts() in controller";
	public static final String LOGIN_START_IN_CONTROLLER = "start processing userLogin() in controller";
	
	public static final String PLACE_ORDER_START_IN_SERVICE = "start processing placeOrder() in service";
	public static final String PLACE_ORDER_END_IN_SERVICE = "end processing placeOrder() in service";
	public static final String ADD_TO_CART_START_IN_SERVICE = "start processing addProductsToCart() in service";
	public static final String ADD_TO_CART_END_IN_SERVICE = "end processing addProductsToCart() in service";
	public static final String SEARCH_PRODUCT_START_IN_SERVICE = "start processing searchProducts() in service";
	public static final String SEARCH_PRODUCT_END_IN_SERVICE = "end processing searchProducts() in service";
	public static final String LOGIN_START_IN_SERVICE = "start processing userLogin() in service";
	public static final String PLACED ="Placed";
	public static final String PLACED_SUCCESS ="Order placed successfully";
	public static final String ADDED ="added";
	public static final String ADDED_SUCCESS ="Product added to the cart";
	public static final String PRODUCT_NOT_AVAILABLE ="Product Not Available";
	public static final String LOGIN_SUCCESS ="Login Success";
	public static final String USER_CREDENTIALS_INCORRECT ="UserId or password are not correct";
	

}
