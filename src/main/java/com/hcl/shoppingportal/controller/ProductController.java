package com.hcl.shoppingportal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.shoppingportal.dto.ProductDto;
import com.hcl.shoppingportal.exception.ProductNotFoundException;
import com.hcl.shoppingportal.service.ProductService;
import com.hcl.shoppingportal.util.ShoppingPortalConstants;

import lombok.extern.slf4j.Slf4j;

/**
 * This controller is handling the requests and responses for product
 * operations.
 * 
 * @author SQUAD 4
 * @since 2020/11/19
 *
 */
@Slf4j
@RestController
@RequestMapping("/")
public class ProductController {

	@Autowired
	ProductService productService;

	/**
	 * Method to call service method to search the products
	 * 
	 * @param productName productName for search
	 * @return List<ProductDto> which consist of list of products.
	 * @throws ProductNotFoundException will throw if search product fails. 
	 */
	@GetMapping("products")
	public ResponseEntity<List<ProductDto>> searchProducts(@RequestParam String productName)
			throws ProductNotFoundException {
		log.info(ShoppingPortalConstants.SEARCH_PRODUCT_START_IN_CONTROLLER);
		List<ProductDto> productDtos = productService.searchProducts(productName);
		log.info(ShoppingPortalConstants.SEARCH_PRODUCT_END_IN_CONTROLLER);
		return new ResponseEntity<>(productDtos, HttpStatus.OK);
	}

}
