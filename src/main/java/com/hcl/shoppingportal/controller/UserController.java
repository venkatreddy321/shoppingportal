package com.hcl.shoppingportal.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.shoppingportal.dto.ResponseDto;
import com.hcl.shoppingportal.service.UserServiceImpl;
import com.hcl.shoppingportal.util.ShoppingPortalConstants;

import lombok.extern.slf4j.Slf4j;

/**
 * This controller is handling the requests and responses for user operations.
 * 
 * @author SQUAD 4
 * @since 2020/11/19
 *
 */
@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	UserServiceImpl userService;

	/**
	 * Method to call service method for login
	 * 
	 * @param userId id of the user
	 * @param pwd    password of the user
	 * @return Optional<UserResponseDto> which consist of status message and code.
	 * 
	 */
	@PostMapping("/login")
	public ResponseEntity<Optional<ResponseDto>> userLogin(@RequestParam Long userId, @RequestParam String pwd) {
		log.info(ShoppingPortalConstants.LOGIN_START_IN_CONTROLLER);

		return new ResponseEntity<>(userService.loginUser(userId, pwd), HttpStatus.OK);

	}

}
