package com.hcl.shoppingportal.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.shoppingportal.dto.PlaceOrderReq;
import com.hcl.shoppingportal.dto.ProductDto;
import com.hcl.shoppingportal.dto.ResponseDto;
import com.hcl.shoppingportal.exception.ProductNotFoundException;
import com.hcl.shoppingportal.service.CartService;
import com.hcl.shoppingportal.util.ShoppingPortalConstants;

import lombok.extern.slf4j.Slf4j;

/**
 * This controller is handling the requests and responses for cart operations.
 * 
 * @author SQUAD 4
 * @since 2020/11/19
 *
 */
@Slf4j
@RestController
@RequestMapping("/")
public class CartController {


	@Autowired
	CartService cartService;

	/**
	 * Method to call service method to place the order in cart
	 * 
	 * @param userId         for add the product in cart.
	 * @param placeOrderReqs for get the product details.
	 * @return ResponseDto which consist of status message and code.
	 * 
	 */
	@PostMapping("placeOrder")
	public ResponseEntity<ResponseDto> placeOrder(@RequestParam Long userId,
			@RequestBody List<PlaceOrderReq> placeOrderReqs) {
		log.info(ShoppingPortalConstants.PLACE_ORDER_START_IN_CONTROLLER);
		ResponseDto responseDto = cartService.placeOrder(userId, placeOrderReqs);
		log.info(ShoppingPortalConstants.PLACE_ORDER_END_IN_CONTROLLER);
		return new ResponseEntity<>(responseDto, HttpStatus.OK);
	}

	/**
	 * Method to call service to add the products in cart.
	 * 
	 * @param userId         for add the product in cart.
	 * @param placeOrderReqs for get the product details.
	 * @return ProductResponseDto which consist of status message and code.
	 * @throws ProductNotFoundException will throw if add to cart fails.
	 */
	@PostMapping("cart/addproductstocart")
	public ResponseEntity<Optional<ResponseDto>> addProducts(@RequestParam Long userId,
			@RequestBody List<PlaceOrderReq> placeOrderReqs) throws ProductNotFoundException {
		log.info(ShoppingPortalConstants.ADD_TO_CART_START_IN_CONTROLLER);

		return new ResponseEntity<>(cartService.addProductsToCart(userId, placeOrderReqs), HttpStatus.CREATED);

	}

	/**
	 * This API is used to retrieve list of placed order for given user id.
	 * 
	 * @param userId Customer user ID
	 * @return Loaded lists of orders.
	 */
	@GetMapping("/getorders")
	public List<ProductDto> getorders(@RequestParam Long userId) {
		List<ProductDto> listDto = new ArrayList<>();

		List<Object> prodList = cartService.getorders(userId);
		if (!prodList.isEmpty()) {
			for (int iloop = 0; iloop < prodList.size(); iloop++) {
				Object[] obj = (Object[]) prodList.get(iloop);
				ProductDto dto = new ProductDto();
				dto.setProductName(obj[1].toString());
				dto.setProductCost(Double.valueOf(obj[4].toString()));
				dto.setProductDescription(obj[2].toString());
				dto.setProductId(Long.valueOf(obj[0].toString()));
				dto.setModel(obj[3].toString());
				listDto.add(dto);
			}
		}
		return listDto;
	}
}

