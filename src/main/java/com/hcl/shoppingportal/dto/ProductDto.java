package com.hcl.shoppingportal.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductDto {

	private Long productId;
	private String productName;
	private String productDescription;
	private String model;
	private Double productCost;

}
