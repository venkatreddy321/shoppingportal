package com.hcl.shoppingportal.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserResponseDto {

	private String message;
	private Integer status;
	
	

}
