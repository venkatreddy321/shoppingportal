package com.hcl.shoppingportal.dto;

public class OrderPlacedResponseDto {

	private String message;
	private Integer statusCode;


	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	

	
}
