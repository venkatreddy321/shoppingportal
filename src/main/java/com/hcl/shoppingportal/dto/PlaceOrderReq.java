package com.hcl.shoppingportal.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PlaceOrderReq {

	private Long productId;
}
