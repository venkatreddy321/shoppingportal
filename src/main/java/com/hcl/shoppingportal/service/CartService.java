package com.hcl.shoppingportal.service;

import java.util.List;
import java.util.Optional;

import com.hcl.shoppingportal.dto.PlaceOrderReq;
import com.hcl.shoppingportal.dto.ResponseDto;
import com.hcl.shoppingportal.exception.ProductNotFoundException;

/**
 * 
 * Service interface for cart operations. The preferred implementation is
 * {@code CartServiceImpl}.
 * 
 * @author SQUAD 4
 * @since 2020/11/19
 *
 */
public interface CartService {


	/**
	 * place the order which is in cart This method is used to place the product
	 * orders
	 * 
	 * @param userId         Customer User Id
	 * @param placeOrderReqs List of place orders
	 * @return Updated response DTO
	 */

	public ResponseDto placeOrder(Long userId, List<PlaceOrderReq> placeOrderReqs);

	/**
	 * This method is used to adds the product to cart.
	 * 
	 * @param userId       Customer user id.
	 * @param productsList List of product
	 * @return Updated Response
	 * @throws ProductNotFoundException
	 */
	public Optional<ResponseDto> addProductsToCart(Long userId, List<PlaceOrderReq> placeOrderReqs)
			throws ProductNotFoundException;


	/**
	 * This API is used to retrieve list of placed order for given user id.
	 * 
	 * @param userId Customer user ID
	 * @return Loaded lists of orders.
	 */
	public List<Object> getorders(Long userId);


}
