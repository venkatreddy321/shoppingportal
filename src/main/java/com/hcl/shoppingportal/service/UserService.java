package com.hcl.shoppingportal.service;

import java.util.Optional;

import com.hcl.shoppingportal.dto.ResponseDto;

/**
 * 
 * Service interface for user operations. The preferred implementation is
 * {@code UserServiceImpl}.
 * 
 * @author SQUAD 4
 * @since 2020/11/19
 *
 */
public interface UserService {
	
	/**
	 * Method for login by using credentials.
	 * 
	 * @param userId id of the user
	 * @param pwd    password of the user
	 * @return Optional<UserResponseDto> which consist of status message and code.
	 * 
	 */
	public Optional<ResponseDto> loginUser(Long userId, String pwd);
}
