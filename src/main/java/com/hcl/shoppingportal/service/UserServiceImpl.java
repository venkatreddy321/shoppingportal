package com.hcl.shoppingportal.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.hcl.shoppingportal.dto.ResponseDto;
import com.hcl.shoppingportal.entity.User;
import com.hcl.shoppingportal.exception.UserNotFoundException;
import com.hcl.shoppingportal.repository.UserRepository;
import com.hcl.shoppingportal.util.ShoppingPortalConstants;

import lombok.extern.slf4j.Slf4j;

/**
 * Implementation of UserService for user login by credentials.
 * 
 * @author SQUAD 4
 * @since 2020/11/19
 */
@Slf4j
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;

	ResponseDto userResponseDto;

	/**
	 * Method for login through the user credentials
	 * 
	 * @param userId id of the user
	 * @param pwd    password of the user
	 * @return Optional<UserResponseDto> which consist of status message and code.
	 * 
	 */
	@Override
	public Optional<ResponseDto> loginUser(Long userId, String pwd) {
		log.info(ShoppingPortalConstants.LOGIN_START_IN_SERVICE);
		Optional<User> user = userRepository.findByUserId(userId);

		if (user.isPresent()) {
			if (user.get().getUserId().equals(userId) && user.get().getPwd().equals(pwd))

				userResponseDto = new ResponseDto();
			userResponseDto.setMessage(ShoppingPortalConstants.LOGIN_SUCCESS);
			userResponseDto.setStatus(HttpStatus.OK.value());
			return Optional.of(userResponseDto);
		} else {
			throw new UserNotFoundException(ShoppingPortalConstants.USER_CREDENTIALS_INCORRECT);
		}

	}

}
