package com.hcl.shoppingportal.service;

import java.util.List;

import com.hcl.shoppingportal.dto.ProductDto;
import com.hcl.shoppingportal.exception.ProductNotFoundException;

/**
 * 
 * Service interface for product operations. The preferred implementation is
 * {@code ProductServiceImpl}.
 * 
 * @author SQUAD 4
 * @since 2020/11/19
 *
 */
public interface ProductService {

	/**
	 * Method to call service method to search the products
	 * 
	 * @param productName productName for search
	 * @return List<ProductDto> which consist of list of products.
	 * @throws ProductNotFoundException will throw if search product fails. 
	 */
	public List<ProductDto> searchProducts(String productName) throws ProductNotFoundException;

}
