package com.hcl.shoppingportal.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.shoppingportal.dto.ProductDto;
import com.hcl.shoppingportal.entity.Product;
import com.hcl.shoppingportal.exception.ProductNotFoundException;
import com.hcl.shoppingportal.repository.ProductRepository;
import com.hcl.shoppingportal.util.ShoppingPortalConstants;

import lombok.extern.slf4j.Slf4j;

/**
 * Implementation of ProductService which will search the products.
 * 
 * @author SQUAD 4
 * @since 2020/11/19
 */
@Slf4j
@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductRepository productRepository;

	/**
	 * Method to search the products
	 * 
	 * @param productName productName for search
	 * @return List<ProductDto> which consist of list of products.
	 * @throws ProductNotFoundException will throw if search product fails.
	 */
	@Override
	public List<ProductDto> searchProducts(String productName) throws ProductNotFoundException {
		log.info(ShoppingPortalConstants.SEARCH_PRODUCT_START_IN_SERVICE);
		List<Product> products = productRepository.findByProductNameContainsIgnoreCase(productName);
		List<ProductDto> productsResponse = new ArrayList<>();
		if (products.isEmpty()) {
			throw new ProductNotFoundException(ShoppingPortalConstants.PRODUCT_NOT_AVAILABLE);
		}
		products.stream().forEach(product -> {
			ProductDto productDto = new ProductDto();
			BeanUtils.copyProperties(product, productDto);
			productsResponse.add(productDto);
		});
		log.info(ShoppingPortalConstants.SEARCH_PRODUCT_END_IN_SERVICE);
		return productsResponse;
	}

}
