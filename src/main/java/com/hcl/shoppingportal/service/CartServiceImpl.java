package com.hcl.shoppingportal.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.hcl.shoppingportal.dto.PlaceOrderReq;
import com.hcl.shoppingportal.dto.ResponseDto;
import com.hcl.shoppingportal.entity.Cart;
import com.hcl.shoppingportal.exception.ProductNotFoundException;
import com.hcl.shoppingportal.repository.CartRepository;
import com.hcl.shoppingportal.util.ShoppingPortalConstants;

import lombok.extern.slf4j.Slf4j;

/**
 * Implementation of CartService which will do the cart related operations.
 * 
 * @author SQUAD 4
 * @since 2020/11/19
 */
@Slf4j
@Service
public class CartServiceImpl implements CartService {
	
	

	@Autowired
	CartRepository cartRepository;

	/**
	 * Method to place the order which is in cart
	 * 
	 * @param userId         for add the product in cart.
	 * @param placeOrderReqs for get the product details.
	 * @return ResponseDto which consist of status message and code.
	 * 
	 */
	@Override
	public ResponseDto placeOrder(Long userId, List<PlaceOrderReq> placeOrderReqs) {

		log.info(ShoppingPortalConstants.PLACE_ORDER_START_IN_SERVICE);
		ResponseDto responseDto = new ResponseDto();

		placeOrderReqs.stream().forEach(placeOrderReq -> {

			Optional<Cart> cart = cartRepository.findByUserIdAndProductId(userId, placeOrderReq.getProductId());
			cart.get().setStatus(ShoppingPortalConstants.PLACED);
			cartRepository.save(cart.get());
			responseDto.setMessage(ShoppingPortalConstants.PLACED_SUCCESS);

		});

		responseDto.setStatus(HttpStatus.OK.value());
		log.info(ShoppingPortalConstants.PLACE_ORDER_END_IN_SERVICE);
		return responseDto;
	}

	/**
	 * Method to add the products in cart.
	 * 
	 * @param userId         for add the product in cart.
	 * @param placeOrderReqs for get the product details.
	 * @return ProductResponseDto which consist of status message and code.
	 * @throws ProductNotFoundException will throw if add to cart fails.
	 */
	@Override
	public Optional<ResponseDto> addProductsToCart(Long userId, List<PlaceOrderReq> placeOrderReqs)
			throws ProductNotFoundException {
		log.info(ShoppingPortalConstants.ADD_TO_CART_START_IN_SERVICE);
		ResponseDto productResponseDto;
		
		placeOrderReqs.stream().forEach(placeOrderReq -> {
			Cart cart = new Cart();
			cart.setUserId(userId);
			cart.setProductId(placeOrderReq.getProductId());
			cart.setStatus(ShoppingPortalConstants.ADDED);
			cartRepository.save(cart);
		});

		productResponseDto = new ResponseDto();
		productResponseDto.setMessage(ShoppingPortalConstants.ADDED_SUCCESS);
		productResponseDto.setStatus(HttpStatus.ACCEPTED.value());
		log.info(ShoppingPortalConstants.ADD_TO_CART_END_IN_SERVICE);
		return Optional.of(productResponseDto);
	}

	/**
	 * This API is used to retrieve list of placed order for given user id.
	 * 
	 * @param userId Customer user ID
	 * @return Loaded lists of orders.
	 */
	@Override
	public List<Object> getorders(Long userId) {
		return cartRepository.getorders(userId);
	}
}
