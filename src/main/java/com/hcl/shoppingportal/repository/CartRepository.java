package com.hcl.shoppingportal.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.hcl.shoppingportal.entity.Cart;


@Repository
public interface CartRepository extends JpaRepository<Cart, Long>{
     
	
	 
	


	Optional<Cart> findByUserIdAndProductId(Long userId, Long productId);
	
	@Query("SELECT p.productId, p.productName, p.productDescription, p.model, p.productCost from Product p, Cart c "
			+ "WHERE  p.productId = c.productId and c.status='placed' and c.userId = :userId")
	public List<Object> getorders( @Param("userId") Long userId);


}
