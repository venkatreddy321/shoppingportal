package com.hcl.shoppingportal.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.hcl.shoppingportal.dto.PlaceOrderReq;
import com.hcl.shoppingportal.dto.ResponseDto;
import com.hcl.shoppingportal.entity.Cart;
import com.hcl.shoppingportal.repository.CartRepository;

@ExtendWith(MockitoExtension.class)
@TestInstance(Lifecycle.PER_CLASS)
public class CartServiceImplTest {

	@InjectMocks
	CartServiceImpl cartServiceImpl;

	@Mock
	CartRepository cartRepository;

	Cart cart;
	ResponseDto responseDto;
	PlaceOrderReq placeOrderReq;
	private final Long userId = 1L;
	private final Long productId = 1L;
	List<PlaceOrderReq> placeOrderReqs;

	@BeforeAll
	public void setup() {
		cart = new Cart();
		cart.setCartId(1L);
		cart.setProductId(1L);
		cart.setStatus("added");
		cart.setUserId(1L);

		placeOrderReq = new PlaceOrderReq();
		placeOrderReq.setProductId(1L);
		placeOrderReqs = new ArrayList<>();
		placeOrderReqs.add(placeOrderReq);
	}

	@Test
	public void testplaceOrder() {
		Mockito.when(cartRepository.findByUserIdAndProductId(userId, productId)).thenReturn(Optional.of(cart));
		ResponseDto result = cartServiceImpl.placeOrder(userId, placeOrderReqs);
		assertEquals("Order placed successfully", result.getMessage());
	}
}
