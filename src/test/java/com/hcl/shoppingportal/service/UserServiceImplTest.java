package com.hcl.shoppingportal.service;

import java.util.Optional;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.hcl.shoppingportal.dto.ResponseDto;
import com.hcl.shoppingportal.entity.User;
import com.hcl.shoppingportal.repository.UserRepository;

@ExtendWith(MockitoExtension.class)
@TestInstance(Lifecycle.PER_CLASS)
public class UserServiceImplTest {

	@Mock
	UserRepository userRepository;
	@InjectMocks
	UserServiceImpl userService;

	static ResponseDto userResponseDto;
	static User user;
	private final Long userId = 123L;
	private final String pwd = "venkatreddy";

	@BeforeEach
	public void setUp() {
		user = new User();
		user.setUserId(123L);
		user.setCreatedBy("vensese");
		user.setFirstName("venkat");
		user.setLastName("reddy");
		user.setModifiedBy("kridh");
		user.setPwd("venkatreddy");
	}

	@Test
	public void loginUserTest() {
		// Given
		Mockito.when(userRepository.findByUserId(user.getUserId())).thenReturn(Optional.of(user));

		// When
		java.util.Optional<ResponseDto> user1 = userService.loginUser(userId, pwd);

		// Then
		Assert.assertNotNull(user1);

	}
}
