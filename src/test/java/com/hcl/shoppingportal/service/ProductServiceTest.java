package com.hcl.shoppingportal.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.hcl.shoppingportal.dto.ProductDto;
import com.hcl.shoppingportal.entity.Product;
import com.hcl.shoppingportal.exception.ProductNotFoundException;
import com.hcl.shoppingportal.repository.ProductRepository;

@ExtendWith(MockitoExtension.class)
@TestInstance(Lifecycle.PER_CLASS)
//@SpringBootTest
public class ProductServiceTest {
	@Mock
	ProductRepository productRepository;
	

	@InjectMocks
	ProductServiceImpl productServiceImpl;
	
	
	 Product product;
	 ProductDto productDto;
	 List<ProductDto> productsDto= new ArrayList<>();
    List<Product> products= new ArrayList<>();
	@BeforeAll
	public  void setup() {
		product= new Product();
		product.setProductId(1L);
		product.setProductName("phone");
		product.setProductDescription("andriod phone");
		product.setModel("s123");
		product.setProductCost(23534.5);
		product.setAvailableQuantity(100);
		products.add(product);
		
		productDto= new ProductDto();
		productDto.setProductId(1L);
		productDto.setProductName("phone");
		productDto.setProductDescription("andriod phone");
		productDto.setModel("s123");
		productDto.setProductCost(23534.5);
		productsDto.add(productDto);
		
}
	@Test
	public void testSearchProduct() throws ProductNotFoundException {
		Mockito.when(productRepository.findByProductNameContainsIgnoreCase(product.getProductName())).thenReturn(products);
		List<ProductDto> result = productServiceImpl.searchProducts(product.getProductName());
		assertNotNull(result);
	}

	@Test
	public void testSearchProductNoProduct() throws ProductNotFoundException {
		List<Product> productsEmplty=new ArrayList<>();
		Mockito.when(productRepository.findByProductNameContainsIgnoreCase("kkk")).thenReturn(productsEmplty);
		List<ProductDto> result = productServiceImpl.searchProducts("kkk");
		assertNotNull(result);
		
	}

}
