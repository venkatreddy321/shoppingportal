package com.hcl.shoppingportal.controller;



import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import com.hcl.shoppingportal.dto.PlaceOrderReq;
import com.hcl.shoppingportal.dto.ProductDto;
import com.hcl.shoppingportal.dto.ResponseDto;
import com.hcl.shoppingportal.exception.ProductNotFoundException;
import com.hcl.shoppingportal.service.CartService;

@ExtendWith(MockitoExtension.class)
@TestInstance(Lifecycle.PER_CLASS)

public class CartControllerTest {

	@Mock
	CartService cartService;

	@InjectMocks
	CartController cartController;

	List<ProductDto> productList;

	ProductDto productDto;

	ResponseDto userResponseDto;
	Long userId = 1234L;

	PlaceOrderReq placeOrderReqs;

	@BeforeAll
	public void setUp() {
		productDto = new ProductDto();
		productDto.setModel("Vivo");
		productDto.setProductCost(100000d);
		productDto.setProductDescription("Mobile phone");
		productDto.setProductId(1l);
		productDto.setProductName("Phone");

		productList = new ArrayList<>();
		productList.add(productDto);

		placeOrderReqs = new PlaceOrderReq();
		placeOrderReqs.setProductId(100L);

		userResponseDto = new ResponseDto();
		userResponseDto.setStatus(HttpStatus.ACCEPTED.value());
	}

	@Test
	public void addProductsToCartTest() throws ProductNotFoundException {
		userResponseDto.setMessage("Product added to the cart");
		Mockito.when(cartService.addProductsToCart(userId, List.of(placeOrderReqs)))
				.thenReturn(Optional.of(userResponseDto));
		ResponseEntity<Optional<ResponseDto>> result = cartController.addProducts(userId, List.of(placeOrderReqs));
		assertEquals("Product added to the cart", result.getBody().get().getMessage());
	}

	@Test
	public void placeOrderTest() throws ProductNotFoundException {
		userResponseDto.setMessage("Order placed successfully");
		Mockito.when(cartService.placeOrder(userId, List.of(placeOrderReqs))).thenReturn(userResponseDto);
		ResponseEntity<ResponseDto> result = cartController.placeOrder(userId, List.of(placeOrderReqs));
		assertEquals("Order placed successfully", result.getBody().getMessage());
	}

	@Test
	@DisplayName("Gets list of placed orders")
	public void getordersNegativeTest() throws ProductNotFoundException {
		List<ProductDto> productDtos = cartController.getorders(userId);
		Assert.notNull(productDtos);
	}


}
