package com.hcl.shoppingportal.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import com.hcl.shoppingportal.dto.ResponseDto;
import com.hcl.shoppingportal.service.UserServiceImpl;

@ExtendWith(MockitoExtension.class)
@TestInstance(Lifecycle.PER_CLASS)
public class UserControllerTest {

	@Mock
	private UserServiceImpl userService;
	@InjectMocks
	private UserController userController;
	private final Long userId = 123L;
	private final String pwd = "venkatreddy";

	ResponseDto userResponseDto;

	@BeforeEach
	public void setUp() {
		userResponseDto = new ResponseDto();
		userResponseDto.setMessage("Login Successfull..");
		userResponseDto.setStatus(200);
	}

	@Test
	public void userLoginTest() {
		Mockito.when(userService.loginUser(userId, pwd)).thenReturn(Optional.of(userResponseDto));
		ResponseEntity<Optional<ResponseDto>> result = userController.userLogin(userId, pwd);
		assertEquals("Login Successfull..", result.getBody().get().getMessage());
	}
}
