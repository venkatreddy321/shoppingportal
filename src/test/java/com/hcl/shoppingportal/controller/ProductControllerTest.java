package com.hcl.shoppingportal.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import com.hcl.shoppingportal.dto.ProductDto;
import com.hcl.shoppingportal.exception.ProductNotFoundException;
import com.hcl.shoppingportal.service.ProductService;

@ExtendWith(MockitoExtension.class)
@TestInstance(Lifecycle.PER_CLASS)
public class ProductControllerTest {
	
	@Mock
	ProductService productService;
	
	@InjectMocks
	ProductController productController;
	
	ProductDto productDto;
	
	List<ProductDto> products;
	
	@BeforeAll
	public void setUp() {
		productDto=new ProductDto();
		productDto.setModel("Vivo");
		productDto.setProductCost(100000d);
		productDto.setProductDescription("Mobile phone");
		productDto.setProductId(1l);
		productDto.setProductName("Phone");
		
		products=new ArrayList<>();
		products.add(productDto);		
		
	}

	@Test
	@DisplayName("Search products by productName")
	public void searchProductsTest() throws ProductNotFoundException {
		
		//GIVEN
		String productName="Phone";
		when(productService.searchProducts(productName)).thenReturn(products);
		
		//WHEN
		ResponseEntity<List<ProductDto>> productDtos=productController.searchProducts(productName);
		
		//THEN
		assertEquals(products.get(0), productDtos.getBody().get(0));
	}
	

}
